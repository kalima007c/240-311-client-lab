let express = require('express');
let bodyParser = require('body-parser');
let router = express.Router();
let cors = require('cors');
let app = express();
app.use(cors());

// app.get('/',(req,res)=>{
//     res.send('Hello World')
// })

app.use('/api', bodyParser.json(), router);
app.use('/api', bodyParser.urlencoded({ extended: false }), router);

let bears = [{'id':0,'name':'pooh','weight': 211},  // --> Create Bears Object
   {'id':1, 'name':'vinnie','weight': 111}
];


router.route('/bears')
.get((req, res) =>  {
    res.json(bears)                                 // --> Response as Json in Browser
})

.post((req,res)=>{
    var bear = {}
    bear.id = bears[bears.length - 1].id+1
    bear.name = req.body.name
    bear.weight = req.body.weight
    bears.push(bear);
    res.json({message : 'bear created'})
})


app.use("*", (req,res) => res.status(404).send('404 Not found') );
app.listen(8000,()=>{
    console.log('Server Running at Port 8000')
})

/*
(Chapter 6 : RPC(Remote Procedure Call) & REST(Representional State Transfer))
        # Command List       
            # npm i --y                                     --> init npm project
            # npm i express --save                          --> install express
            # npm i nodemon --save                          --> install nodemon
            # ./node_modules/.bin/nodemon index.js          --> execute nodemon
        # Plugin
            # json viewer extension (Chrome)
*/