let express = require("express");
let bodyParser = require("body-parser");
let cors = require("cors");
let router = express.Router();
let app = express();

app.use(cors());
app.use("/api", bodyParser.json(), router);
app.use("/api", bodyParser.urlencoded({ extended: false }, router));

let students = [
    {   "id": '5935512007',
        "name" : "Donnukrit",
        "surname": "Satirakul",
        "major": "CoE",
        "gpa": 3.6
    }
];

// route page and response as json
router.route("/students") // create
    .get((req, res) => res.json(students))
    // insert new student
    .post((req,res)=>{
        let student = {}
        student.id = parseInt.students[students.length - 1].id+1 // increment new id
        student.name = req.body.name
        student.surname = req.body.surname
        student.major = req.body.major
        student.gpa = req.body.gpa
        students.push(student)
        res.json({message : 'Student has been create'})
    })

router.route('/students/:std_id')
    .get((req,res)=>{ // show  data get student
        let id = req.params.std_id
        let index = students.findIndex(student => (students.id === +id) )
        res.json(students[index])
    })
    .put((req,res)=>{ // update data
        let id = req.params.std_id
        let index = students.findIndex(student => (student.id === +id) )
        students[index].name = req.body.name
        students[index].surname = req.body.surname
        students[index].major = req.body.major
        students[index].gpa = req.body.gpa
        res.json({message : 'Student has been updated : '+req.params.std_id} )
    })
    .delete((req,res)=>{ // delete
        let id = req.params.std_id
        let index = students.findIndex(student => (student.id === +id))
        students.splice(index,1)
        res.json({message : 'Student has been deleted : '+req.params.std_id} )
    })

app.listen(5555, () => console.log("Server is Running on localhost:5555"))
