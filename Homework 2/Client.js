var net = require('net')
var HOST = '127.0.0.1'
var PORT = 5555
var client = new net.Socket();


client.connect(PORT,HOST,function(){
    console.log('CONNECTED TO: ' + HOST + ':' + PORT)
    client.write('5935512007')
})

client.on('data',function(data){
    console.log('Status : '+data.toString())

    if(data.toString() === 'OK') {
        client.write(parseInt(Math.floor(Math.random() * 21)).toString())
    }
    else if(data.toString() === 'BINGO') {
        console.log('BINGO')
        client.destroy()
    }
    else if(data.toString() === 'WRONG')
    {
        client.write(parseInt(Math.floor(Math.random() * 21)).toString())
        console.log('WRONG')
    }
    else if(data.toString() === 'END') {
        client.destroy()
    }
    
})

client.on('close',function(){
    console.log('Connection closed');
})

client.on('error',(data)=>{
    console.log('Error MSG')
})